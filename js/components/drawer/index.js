import React, {PureComponent} from 'react';
import {
    View,
    ScrollView
} from 'react-native';


import styles from './styles';
import MenuItem from './menu-item';


export class DrawerMenu extends PureComponent {
    update = () => {
        if (!this.props.isFetching) {
            this.props.fetchStocks()
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <MenuItem onPress={this.update}
                              title={!this.props.isFetching ? 'Обновить' : 'Обновление...'}/>
                </ScrollView>
            </View>
        )
    }
}


//
import {connect} from 'react-redux';
import {fetchStocks} from '../../actions/stocks';


const mapStateToProps = (state) => {
    return {
        isFetching: state.stocksState.isFetching
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchStocks: () => dispatch(fetchStocks())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu);

import React, {PureComponent} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

import styles from './styles';

const MenuItem = ({title, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.menuItemWrapper}>
                <Text style={styles.menuItemText}>
                    {title}
                </Text>
            </View>
        </TouchableOpacity>
    )

};


export default MenuItem;
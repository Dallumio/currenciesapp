import {
    StyleSheet
} from 'react-native';


export default styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingTop: 50,
        flex: 1
    },

    menuItemWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 15,
        backgroundColor: '#6e0fd6',
        paddingVertical: 10,
        paddingLeft: 15
    },
    menuItemText: {
        color: 'white'
    }

})
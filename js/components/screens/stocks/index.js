import React, {PureComponent} from 'react';
import {View, FlatList} from 'react-native'


import Currency from './currency';
import Controls from './controls';
import styles from './styles';

export class StocksScreen extends PureComponent {

    _keyExtractor = (item, index) => item.name;

    _renderItem = ({item}) => (
        <Currency
            name={item.name}
            price={item.price.amount}
            volume={item.volume}/>
    );

    render() {
        const {
            stocks, isFetching, isError, updatedAt,
            fetchStocks
        } = this.props;

        return (
            <View style={styles.wrapper}>
                <Controls isFetching={isFetching}
                          isError={isError}
                          updatedAt={updatedAt}
                          update={fetchStocks}
                />
                <FlatList
                    data={stocks}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}


//
import {connect} from 'react-redux';

import {fetchStocks} from '../../../actions/stocks';


const mapStateToProps = (state) => {
    return {
        stocks: state.stocksState.stocks,
        isFetching: state.stocksState.isFetching,
        isError: state.stocksState.isError,
        updatedAt: state.stocksState.updatedAt,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchStocks: () => dispatch(fetchStocks())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(StocksScreen);

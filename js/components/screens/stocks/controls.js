import React, {PureComponent} from 'react';
import {View, Text, ActivityIndicator, TouchableOpacity} from 'react-native'
import moment from 'moment';

import styles from './styles';

export default class Controls extends PureComponent {
    update = () => {
        if (!this.props.isFetching) {
            this.props.update();
        }
    }

    render() {
        const {isFetching, isError, updatedAt} = this.props;

        return (
            <View style={styles.controlsBlock}>
                <TouchableOpacity onPress={this.update}>
                    <View style={styles.updateButton}>
                        <Text style={styles.updateButtonText}>{isFetching ? 'Обновление...' : 'Обновить'}</Text>
                    </View>
                </TouchableOpacity>
                <View>
                    <Text>Последнее
                        обновление: {updatedAt ? moment.unix(updatedAt).format('DD.MM HH:mm:ss') : 'не обновлялось'}</Text>
                </View>
                {isFetching ?
                    <ActivityIndicator size="small" color="#00ff00"/>
                    : null}
                {isError ?
                    <View style={styles.errorBlock}>
                        <Text style={styles.errorText}>Ошибка обновления</Text>
                    </View>
                    : null}
            </View>
        )
    }
}
import {
    StyleSheet
} from 'react-native';


export default styles = StyleSheet.create({
    wrapper: {
        paddingHorizontal: 10,
        backgroundColor: '#ddd'
    },
    currencyBlock: {
        backgroundColor: 'white',
        paddingVertical: 3,
        paddingHorizontal: 5,
        marginBottom: 3
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50
    },
    rowTitle: {
        fontWeight: 'bold'
    },
    rowValue: {
        marginLeft: 5
    },
    controlsBlock: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginBottom: 5
    },
    updateButton: {
        backgroundColor: '#6e0fd6',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 5
    },
    updateButtonText: {
        color: 'white'
    },
    errorBlock: {},
    errorText: {
        color: 'red'
    }

})
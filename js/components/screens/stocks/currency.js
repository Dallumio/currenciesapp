import React, {PureComponent} from 'react';
import {View, Text} from 'react-native'

import styles from './styles';

export default class Currency extends PureComponent {
    render() {
        const {name, volume, price} = this.props;

        return (
            <View style={styles.currencyBlock}>
                <View style={styles.row}>
                    <Text style={styles.rowTitle}>Валюта:</Text>
                    <Text style={styles.rowValue}>{name}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.rowTitle}>Цена:</Text>
                    <Text style={styles.rowValue}>{price}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.rowTitle}>Объем:</Text>
                    <Text style={styles.rowValue}>{volume}</Text>
                </View>
            </View>
        )
    }
}
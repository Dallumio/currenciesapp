import {
    REQUEST_STOCKS,
    RECEIVE_STOCKS_ERROR,
    RECEIVE_STOCKS_SUCCESS
} from '../actions/stocks';
import moment from 'moment';

export default function stocks(state = {
    stocks: [],
    isFetching: false,
    isError: false,
    updatedAt: 0
}, action) {
    switch (action.type) {
        case REQUEST_STOCKS: {
            return Object.assign({}, state, {
                isError: false,
                isFetching: true
            })
        }
        case RECEIVE_STOCKS_SUCCESS: {
            const stocks = action.stocks;

            return Object.assign({}, state, {
                stocks: stocks,
                isFetching: false,
                isError: false,
                updatedAt: moment().unix()
            })
        }
        case RECEIVE_STOCKS_ERROR: {
            console.log('Error', action.err);
            return Object.assign({}, state, {
                isFetching: false,
                isError: true
            })
        }

        default:
            return state
    }
}
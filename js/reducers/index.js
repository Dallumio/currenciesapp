import {combineReducers} from 'redux'




import stocks from './stocks';

const appReducer = combineReducers({
    stocksState: stocks,
});

export default appReducer;
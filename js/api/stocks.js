import {Request} from './api';


export default class {

    static async getStocks() {
        let response = await Request.get('/stocks.json');
        return response;
    }
}


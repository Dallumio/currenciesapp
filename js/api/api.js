const API_URL = 'http://phisix-api3.appspot.com';

const API_HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};


import
{
    NetInfo
}
    from 'react-native';

import qs from 'qs';
import constants from '../constants';


class Request {


    static async get (url, params) {
        let result = await this.fetch({url: url, params: qs.stringify(params), type: 'get'});
        return result;
    }

    static async post(url, params) {
        let result = await this.fetch({
            url: url,
            params: params,
            type: 'post',
        });
        return result;
    }

    static async fetch(data) {
        var isNoConnection = false;

        var result = await NetInfo.getConnectionInfo();
        if (result == 'none' || result == 'NONE') {
            return {err: constants.ERRORS.NO_CONNECTION}
        }
        try {
            let response = null;
            if (data.type == 'get') {
                response = await fetch(API_URL + data.url);
            }
            if (data.type == 'post') {
                response = await fetch(API_URL + data.url, {
                    method: 'POST',
                    headers: API_HEADERS,
                    body: JSON.stringify(data.params)
                });
            }

            if (!response) {
                return {err: constants.ERRORS.NO_CONNECTION};
            }

            setTimeout(() => null, 0); // React-native bug workaround
            let responseJson = await response.json();
            return responseJson;
        } catch (err) {
            return {err: constants.ERRORS.UNHANDLED_EXCEPTION};
        }
    }
}

export {API_URL, API_HEADERS, Request};

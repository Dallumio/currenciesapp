export default {
    ERRORS: {
        UNHANDLED_EXCEPTION: 100,
        NO_CONNECTION: 101,
        NO_RESPONSE: 102
    }
}
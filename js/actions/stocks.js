import STOCKS_API from '../api/stocks';


export const REQUEST_STOCKS = 'REQUEST_STOCKS';
export const RECEIVE_STOCKS_SUCCESS = 'RECEIVE_STOCKS_SUCCESS';
export const RECEIVE_STOCKS_ERROR = 'RECEIVE_STOCKS_ERROR';


export const requestStocks = () => (
    {type: REQUEST_STOCKS}
);

export const receiveStocksSuccess = (stocks) => (
    {
        type: RECEIVE_STOCKS_SUCCESS,
        stocks: stocks
    }
);

export const receiveStocksError = (err) => (
    {
        type: RECEIVE_STOCKS_ERROR,
        err: err
    }
);



export const fetchStocks = () => {
    return async (dispatch) => {
        dispatch(requestStocks());

        let response = await STOCKS_API.getStocks()
        console.log(response);
        if (response.err) {
            dispatch(receiveStocksError(response.err));
        } else {
            dispatch(receiveStocksSuccess(response.stock));
        }


    }
}
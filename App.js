import React from 'react';
import {DrawerLayoutAndroid, Platform} from 'react-native';
import BackgroundTimer from 'react-native-background-timer';


import DrawerLayout from 'react-native-drawer-layout-polyfill';

const DrawerLayoutWrapper = Platform.OS == 'android' ? DrawerLayoutAndroid : DrawerLayout;

import {Provider} from 'react-redux';


import {fetchStocks} from './js/actions/stocks';
import configureStore from './js/store';


import StocksScreen from './js/components/screens/stocks';
import DrawerMenu from './js/components/drawer'

const UPDATE_TIMEOUT = 15000;

class ReduxApp extends React.Component {
    _store = configureStore();

    updateStocks = () => {
        this._store.dispatch(fetchStocks());

    }

    componentDidMount() {
        this.updateStocks();
        BackgroundTimer.runBackgroundTimer(() => {
            this.updateStocks();
        }, UPDATE_TIMEOUT);
    }

    render() {
        return (
            <Provider store={this._store}>
                <DrawerLayoutWrapper
                    drawerWidth={300}
                    ref={(ref) => this._drawer = ref}
                    drawerPosition={DrawerLayoutWrapper.positions.Right}
                    drawerLockMode={"unlocked"}
                    renderNavigationView={() => <DrawerMenu store={this._store}/>}>
                    <StocksScreen/>
                </DrawerLayoutWrapper>

            </Provider>
        );
    }


}


export default ReduxApp;